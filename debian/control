Source: libreflections-java
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Pierre Gruet <pgtdebian@free.fr>
Build-Depends:
 debhelper-compat (= 13),
 default-jdk-headless,
 javahelper,
 junit4 <!nocheck>,
 libdom4j-java,
 libgoogle-gson-java,
 libjavassist-java,
 libservlet-api-java,
 libslf4j-java,
 maven-debian-helper
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/java-team/libreflections-java
Vcs-Git: https://salsa.debian.org/java-team/libreflections-java.git
Homepage: https://github.com/CampagneLaboratory/icb-utils
Rules-Requires-Root: no

Package: libreflections-java
Architecture: all
Depends:
 libdom4j-java,
 libgoogle-gson-java,
 libjavassist-java,
 libservlet-api-java,
 libslf4j-java,
 ${java:Depends},
 ${misc:Depends}
Description: Java runtime metadata analysis library
 Reflections scans a classpath, indexes the metadata, allows one to query it on
 runtime and may save and collect that information for many modules within a
 project.
 .
 Using Reflections one can query metadata such as:
  - get all subtypes of some type;
  - get all types/members annotated with some annotation;
  - get all resources matching a regular expression;
  - get all methods with specific signature including parameters, parameter
  annotations and return type.
